<!DOCTYPE html>
<html lang="en">

<head>
    <title>insert form</title>
</head>

<body>
    <h1>insert form</h1>


    <div class="container">
        <h1>Transaction List</h1>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Grand Total</th>
                    <th>Date</th>

                    <!-- Add other table headers as needed -->
                </tr>
            </thead>
            <tbody>
                @foreach ($transactions as $key => $transaction)
                    <tr>
                        <td>{{ $transaction['_id'] }}</td>
                        <td>{{ $transaction['grand_total'] }}</td>
                        <td>{{ \Carbon\Carbon::parse($transaction['timestamp'])->format('Y-m-d H:i:s') }}
                        </td>
                        <!-- Add other table columns as needed -->

                        @php
                            $time = \Carbon\Carbon::now('Asia/Vientiane')->format('Y-m-d');
                            // dd($time);
                        @endphp
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</body>

</html>
