<?php

namespace App\Http\Controllers;

use App\Models\Insert;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;

class InsertController extends Controller
{
    public function view()
    {

        $apiUrl = 'https://go.pospos.co/developer/api/transactions?page=1&limit=200&start=2023-09-22&end=2023-09-22';
        $apiKey = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxZmU0YmY1OWRjYzExMmZjOTc1N2JlNiIsImVtcGxveW1lbnRfdHlwZSI6IjViZDZjOGVkZTFhNDM3NTgwNzkwODUwZiIsInNob3AiOiIxNjg1MTcyNDE0NDY3IiwidmVyc2lvbiI6IjEuMCIsImxldmVsIjoxLCJpYXQiOjE2OTUyNzg5OTYsImV4cCI6NDg1MTAzODk5NiwiYXVkIjoiaHR0cHM6Ly9wb3Nwb3Nnby5jbyIsImlzcyI6IkNvZGVNb2JpbGVzIEx0ZCJ9.GO5kLgH3aBGjzFCBSr1qOYo7xOGM0dh5L_IE3rOEDmYbI8RAFOL9RVcNYVeYzlwFaL_5OfRwfE6IZwAluDBYqWkE6OvyiDKl_3M-5_MF1qFPIlll4p7ZMhF1DWPh2aPRKpVgUO26wRcSXIO4yK0h_MJFrJu-O1j7b22yIY4p99g'; // Replace with your actual API key

        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $apiKey,
        ])->get($apiUrl);


        if ($response->successful()) {
            $data = $response->json();

            // Now you can work with the API response data
            // For example, you can access the "data" key of the response like this:
            $transactions = $data['data'];
            // dd($transactions);

            // Perform actions with the $transactions data here
        } else {
            // Handle the case where the API request was not successful
            // You can access error information using $response->status() and $response->body()
            $statusCode = $response->status();
            $errorBody = $response->body();
            $transactions = [];

            // Handle the error appropriately
        }

        return view('admin.view', compact('transactions'));
    }



    public function insert()
    {

        $apiUrl = 'https://go.pospos.co/developer/api/transactions?page=1&limit=200&start=2023-09-22&end=2023-09-22';
        $apiKey = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxZmU0YmY1OWRjYzExMmZjOTc1N2JlNiIsImVtcGxveW1lbnRfdHlwZSI6IjViZDZjOGVkZTFhNDM3NTgwNzkwODUwZiIsInNob3AiOiIxNjg1MTcyNDE0NDY3IiwidmVyc2lvbiI6IjEuMCIsImxldmVsIjoxLCJpYXQiOjE2OTUyNzg5OTYsImV4cCI6NDg1MTAzODk5NiwiYXVkIjoiaHR0cHM6Ly9wb3Nwb3Nnby5jbyIsImlzcyI6IkNvZGVNb2JpbGVzIEx0ZCJ9.GO5kLgH3aBGjzFCBSr1qOYo7xOGM0dh5L_IE3rOEDmYbI8RAFOL9RVcNYVeYzlwFaL_5OfRwfE6IZwAluDBYqWkE6OvyiDKl_3M-5_MF1qFPIlll4p7ZMhF1DWPh2aPRKpVgUO26wRcSXIO4yK0h_MJFrJu-O1j7b22yIY4p99g'; // Replace with your actual API key

        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $apiKey,
        ])->get($apiUrl);


        if ($response->successful()) {
            $data = $response->json();

            // Now you can work with the API response data
            // For example, you can access the "data" key of the response like this:
            $transactions = $data['data'];
            // dd($transactions);

            // Perform actions with the $transactions data here
        } else {
            // Handle the case where the API request was not successful
            // You can access error information using $response->status() and $response->body()
            $statusCode = $response->status();
            $errorBody = $response->body();
            $transactions = [];

            // Handle the error appropriately
        }

        foreach ($transactions as $transaction) {

            foreach ($transactions as $transactionData) {
                // Assuming the API data structure matches your table structure
                $transaction = new Insert();
                $transaction->invoice = $transactionData['_id'];
                $transaction->date = Carbon::parse($transaction['timestamp'])->format('Y-m-d H:i:s');
                $transaction->grand_total = $transactionData['grand_total'];
                // Set other fields as needed
                $transaction->save();
            }

            if ($transaction) {
                dd('ok');
            }
        }
    }
}
