<?php

namespace App\Console\Commands;

use App\Models\Insert;
use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class InsertCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insertData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert Data from Api';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $date = Carbon::now('Asia/Vientiane')->format('Y-m-d');
        // $apiUrl = 'https://go.pospos.co/developer/api/transactions?page=1&limit=200&start=2023-10-01&end=2023-10-01';
        $apiUrl = 'https://go.pospos.co/developer/api/transactions?page=1&limit=200&start='.$date.'&end='.$date;

        $apiKey = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxZmU0YmY1OWRjYzExMmZjOTc1N2JlNiIsImVtcGxveW1lbnRfdHlwZSI6IjViZDZjOGVkZTFhNDM3NTgwNzkwODUwZiIsInNob3AiOiIxNjg1MTcyNDE0NDY3IiwidmVyc2lvbiI6IjEuMCIsImxldmVsIjoxLCJpYXQiOjE2OTUyNzg5OTYsImV4cCI6NDg1MTAzODk5NiwiYXVkIjoiaHR0cHM6Ly9wb3Nwb3Nnby5jbyIsImlzcyI6IkNvZGVNb2JpbGVzIEx0ZCJ9.GO5kLgH3aBGjzFCBSr1qOYo7xOGM0dh5L_IE3rOEDmYbI8RAFOL9RVcNYVeYzlwFaL_5OfRwfE6IZwAluDBYqWkE6OvyiDKl_3M-5_MF1qFPIlll4p7ZMhF1DWPh2aPRKpVgUO26wRcSXIO4yK0h_MJFrJu-O1j7b22yIY4p99g'; // Replace with your actual API key

        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $apiKey,
        ])->get($apiUrl);


        if ($response->successful()) {
            $data = $response->json();
            $transactions = $data['data'];
            // dd($transactions);
            foreach ($transactions as $transaction) {
                foreach ($transactions as $transactionData) {
                    // $transaction = new Insert();
                    // $transaction->invoice = $transactionData['_id'];
                    // $transaction->date = Carbon::parse($transaction['timestamp'])->format('Y-m-d H:i:s');
                    // $transaction->grand_total = $transactionData['grand_total'];

                    // $transaction->save();

                    $existingRecord = Insert::where('invoice', $transactionData['_id'])->first();

                    if (!$existingRecord) {
                        // Record doesn't exist, insert it
                        Insert::create([
                            'invoice' => $transactionData['_id'],
                            'grand_total' => $transactionData['grand_total'],
                            'date' => Carbon::parse($transaction['timestamp'])->format('Y-m-d H:i:s'),

                        ]);
                    }
                    $this->info('Database inserted successfully.');
                }
            }
        } else {
            $statusCode = $response->status();
            $errorBody = $response->body();
            $transactions = [];
        }
    }
}
